/*
 * COPYRIGHT NOTICE
 * © 2019  Transsmart Holding B.V.
 * All Rights Reserved.
 * All information contained herein is, and remains the
 * property of Transsmart Holding B.V. and its suppliers,
 * if any.
 * The intellectual and technical concepts contained herein
 * are proprietary to Transsmart Holding B.V. and its
 * suppliers and may be covered by European and Foreign
 * Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written
 * permission is obtained from Transsmart Holding B.V.
 */
package com.transsmart.carrier.data.importer.xpf;

import com.transsmart.carrier.data.importer.base.BaseCarrierDataImporter;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author diogo.a.silva
 */
public class XpfRoutingDataImporter {

    public static void main(String[] args) throws IOException, SQLException {
        BaseCarrierDataImporter carrierDataImporter
                = new BaseCarrierDataImporter("XPF");
        carrierDataImporter.run();
    }

}
